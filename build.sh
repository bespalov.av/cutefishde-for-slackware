#!/bin/bash

for i in \
lxqt-build-tools \
libqtxdg \
fishui \
cutefish-core \
cutefish-settings \
libcutefish \
cutefish-dock \
cutefish-launcher \
cutefish-filemanager \
cutefish-qt-plugins \
cutefish-statusbar \
cutefish-kwin-plugins \
cutefish-calculator \
cutefish-wallpapers \
cutefish-icons \
; do
cd $i || exit 1
./$i.SlackBuild || exit 1
cd ..
done
